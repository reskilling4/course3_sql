You have been hired by the prestigious Nordic Haven Hotel Chain to develop a comprehensive database system for managing their hotel operations efficiently. The hotel chain is known for its high standards, and your task is to design a database that caters to their specific business needs.

A business analyst has conducted an initial interview to understand the Nordic Haven Hotel Chain’s business processes.

## Interview with the Top Manager of Nordic Haven Hotel Chain

Business Analyst (BA): Good morning! Thank you for taking the time to meet with me today. I'm excited to discuss how we can enhance your hotel chain's operations through a relationship database. To start, could you provide an overview of your current business processes and key areas where you see room for improvement?

Hotel Owner (HO): Good morning! Of course, happy to be here. Our hotel chain operates in various locations, serving both business and leisure travelers. We have a range of services, from accommodations to dining and event spaces. Currently, we manage bookings, customer data, and inventory manually, which can be quite challenging. We're looking for ways to streamline our operations and enhance the overall guest experience.

BA: That's a great starting point. To better understand your business, let's break it down. Can you walk me through the main components of your operations, from the initial guest inquiry to their departure?

HO: Certainly. It all begins with reservations and bookings, whether made online or through direct inquiries. Once guests arrive, we handle check-ins, room assignments, and various amenities they might request during their stay. We also manage events, dining reservations, and other services. Post-stay, we handle check-outs and billing and maintain a guest history for loyalty programs.

BA: Got it. Now, in terms of your team, how is the staff organized to manage these processes? Are there specific roles or departments responsible for different aspects?

HO: Yes, we have front desk staff for check-ins and check-outs, reservations teams for bookings, and separate teams for events, dining, and housekeeping. Our challenge lies in the coordination and communication between these departments, as information often needs to be shared seamlessly to ensure a smooth guest experience.

BA: I see. To address these challenges and improve efficiency, a relationship database could be beneficial. We can design it to centralize information, enabling real-time updates and better communication between departments. What specific types of data are crucial for your operations, and how do you envision this information being accessed and utilized?

HO: Key data includes guest profiles, booking details, room availability, and service preferences. Integration between departments is crucial, so having a system where staff can access and update this information in real time would be a game-changer. It would also be great to have historical data on guest preferences to enhance personalized services and loyalty programs.

BA: Excellent. Understanding the data elements and the need for real-time access is crucial. Now, let's discuss scalability. Do you have plans for expanding your hotel chain, and how should the relationship database accommodate potential growth?

HO: Expansion is definitely on the horizon. We're looking to add more locations and possibly introduce new services. The database should be flexible enough to adapt to the changing needs of our growing business. It should also support data analytics to provide insights into guest trends and preferences, helping us make informed decisions.

## Task 1: Conceptual ER Diagram

Create a conceptual ER diagram.

## Task 2: Logical ER Diagram

Translate the conceptual ER diagram into a logical ER diagram, applying normalization principles. Establish tables for entities, defining primary and foreign keys to ensure a robust structure with minimal redundancy.

## Task 3: Physical ER Diagram with Design Choices
_
Develop a physical ER diagram based on the logical design. Specify data types, constraints, and indexing strategies. Justify your design choices.

You are NOT required to submit any SQL code which physically creates the tables, the diagrams and description will communicate your decisions.

## Submission
Submit all the appropriate diagrams via a git repository. This assignment should be submitted in groups of three. Ensure all team members’ names are included in the repository.

